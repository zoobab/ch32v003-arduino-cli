// LED is on PC1 (pin1 on CH32V003A4M6 SOP16), so we use C1 as pin name
#define LED_BUILTIN C1
#define MY_DELAY 1000

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(MY_DELAY);
    digitalWrite(LED_BUILTIN, LOW);
    delay(MY_DELAY);
}
