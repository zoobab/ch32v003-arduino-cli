FROM debian:12-slim

RUN apt update && apt install -y ca-certificates libusb-1.0-0-dev

RUN mkdir -pv /root/workdir

WORKDIR /root/workdir

COPY arduino-cli /bin/arduino-cli
RUN chmod +x /bin/arduino-cli
RUN arduino-cli config init --additional-urls https://alexandermandera.github.io/arduino-wch32v003/package_ch32v003_index.json
RUN arduino-cli core install alexandermandera:wch

#COPY bin/blink/blink.ino /root/blink/blink.ino
#RUN arduino-cli compile -v --fqbn alexandermandera:wch:wch32v003 blink
