# Arduino-cli for CH32V003

Arduino-cli with support for the 10 cents CH32V003 chip.

# Status

Right now the docker image does not work because:

1. there is an old version of minichlink inside the Arduino SDK for CH32V003 which does not support Ardulink, see https://github.com/AlexanderMandera/arduino-wch32v003/issues/15
2. there is a permission issue of the minichlink binary, see https://github.com/AlexanderMandera/arduino-wch32v003/issues/14

# Blink

The traditional [blink](code/blink/blink.ino) example using C1 as a blink pin.

# Uart

Helloworld on the serial [uart](code/uart/uart.ino) console at 9600 bauds.

# Docker image

```
$ docker pull registry.gitlab.com/zoobab/ch32v003-arduino-cli

```

# CH32V003A4M6 (SOP16 format)

The pinout diagram is provided by Tengo10:

https://github.com/Tengo10/pinout-overview/blob/main/pinouts/CH32v003/ch32v003a4m6.svg

Here converted to PNG format (with Krita, with ImageMagick convert it was not giving a good result):

![CH32V003A4M6 pinout diagram](ch32v003a4m6.png)

# Todo

* Add board definitions for a SOP16 breakout (SOP20 and SOP8 as well)
* Add Fritzing support for the setups
* Add support for Ardulink
* Static builds of the toolchain, and tools, unusable under Alpine
* Document docker run commands (compile, flash)

# Links

* Arduino port for CH32V003: https://github.com/AlexanderMandera/arduino-wch32v003
* Ardulink: https://gitlab.com/BlueSyncLine/arduino-ch32v003-swio
* Arduino-cli getting started: https://arduino.github.io/arduino-cli/0.35/getting-started/
* Datasheet: https://www.makerwitawat.com/wp-content/uploads/2023/05/CH32V003RM-e_unlocked.pdf
* #13 [QuickStart] GPIO and UART of CH32V003 using Arduino IDE https://www.youtube.com/watch?v=9tLTioG5xQE
